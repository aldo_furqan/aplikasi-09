package furqan.albarado.appx9

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context : Context) : SQLiteOpenHelper(context, DB_Name,null, DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tMp = "create table mp(id_music text, id_cover text, music_title text)"
        val insMp = "insert into mp values('0x7f0c0000', '0x7f06005f', 'Linkin Park - In The End'), " +
                "('0x7f0c0001', '0x7f060060', 'Linkin Park - Numb'), " +
                "('0x7f0c0002', '0x7f060061', 'Linkin Park - What Ive Done')"
        db?.execSQL(tMp)
        db?.execSQL(insMp)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "appx9"
        val DB_Ver = 1
    }
}