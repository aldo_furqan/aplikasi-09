package furqan.albarado.appx9

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaController2
import android.media.MediaPlayer
import android.media.session.MediaController
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(posLaguSkrg)
                playMusik()
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }
    val daftarLagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)
    val daftarVideo = intArrayOf(R.raw.vid_1, R.raw.vid_2, R.raw.vid_3)
    val daftarCover = intArrayOf(R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3)
    val daftarJudul = arrayOf("In The End","Numb","What I've Done")

    var posLaguSkrg = 0
    var posVidSkrg = 0
    var handler = Handler()
    lateinit var adapter : ListAdapter
    lateinit var db : SQLiteDatabase
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: android.widget.MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase
        mediaController = android.widget.MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
//        mediaController.setPrevNextListeners(nextVid,prevVid)
//        mediaController.setAnchorView(videoView2)
//        videoView2.setMediaController(mediaController)
//        videoSet(posVidSkrg)
        lsMp.setOnItemClickListener(itemClicked)
    }

//    var nextVid = View.OnClickListener { v:View ->
//        if (posVidSkrg<(daftarVideo.size-1)) posVidSkrg++
//        else posVidSkrg = 0
//        videoSet(posVidSkrg)
//    }
//
//    var prevVid = View.OnClickListener { v:View ->
//        if (posVidSkrg>0) posVidSkrg--
//        else posVidSkrg = daftarVideo.size-1
//        videoSet(posVidSkrg)
//    }
//
//    fun videoSet(pos : Int){
//        videoView2.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
//    }

    fun milliSecondToString(ms : Int):String{
        var detik : Long = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit : Long = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun playMusik(){
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioPlay(pos : Int){
        val c: Cursor = lsMp.adapter.getItem(pos) as Cursor
        var id_music = c.getInt(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul = c.getString(c.getColumnIndex("music_title"))
        mediaPlayer = MediaPlayer.create(this, id_music)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(id_cover)
        txJudulLagu.setText(judul)
    }

    fun audioNext(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg<(daftarLagu.size-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
        playMusik()
    }

    fun audioPrev(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = daftarLagu.size-1
        }
        audioPlay(posLaguSkrg)
        playMusik()
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    fun showDataMedia(){
        val cursor : Cursor = db.query("mp", arrayOf("id_music as _id","id_cover","music_title"),null,null,null,null,"id_music asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_musik,cursor,
            arrayOf("_id","id_cover","music_title"), intArrayOf(R.id.txtIdMusik, R.id.txtIdCover, R.id.txtMusikTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMp.adapter = adapter

    }

    val itemClicked = AdapterView.OnItemClickListener { parent, view, position, id ->
        posLaguSkrg=position
        audioStop()
        audioPlay(position)
    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onStart() {
        super.onStart()
        showDataMedia()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime : Int = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }


}
